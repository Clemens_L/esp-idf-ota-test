LOCALREPO_VC_DIR=~/esp/esp-idf/.git

if [ ! -d $LOCALREPO_VC_DIR ]
then
    git clone --recursive https://github.com/espressif/esp-idf.git
else
    cd ~/esp/esp-idf
    git pull
fi